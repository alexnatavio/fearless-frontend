function createCard(name, description, pictureUrl, starts, ends, location) {
    const formattedStartDate = new Date(starts).toLocaleDateString('en-US');
    const formattedEndDate = new Date(ends).toLocaleDateString('en-US');

    return `
      <div class="col">
        <div class="card" style="box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-name">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h4>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">
            <p class="card-dates">${formattedStartDate} - ${formattedEndDate}</p>
          </div>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Error occurred when fetching")
        displayAlert("Error occurred when fetching", "alert-danger");
      } else {
        const data = await response.json();

        const conferenceRow = document.getElementById('conferenceRow');

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const ends = details.conference.ends;
            const location = details.conference.location.name;

            console.log('Starts:', starts);
            console.log('Ends:', ends);

            const html = createCard(name, description, pictureUrl, starts, ends, location);
            conferenceRow.innerHTML += html;
          }
        }
      }
    } catch (e) {
      console.log("Error occurred when processing")
      displayAlert("Error occurred when processing", "alert-danger");
    }
  });

  function displayAlert(message, alertType) {
    const alertContainer = document.getElementById('alertContainer');

    const alertDiv = document.createElement('div');
    alertDiv.className = `alert ${alertType}`;
    alertDiv.textContent = message;

    alertContainer.appendChild(alertDiv);
  }
